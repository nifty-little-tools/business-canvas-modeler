/**
 * Markdown renderer instance.
 * @type {MarkdownIt}
 */
const md = window.markdownit();

/**
 * Updates the preview of the editor with the rendered Markdown content.
 * @param {HTMLTextAreaElement} editor - The editor element.
 */
function updatePreview(editor) {
    const previewDiv = editor.nextElementSibling;
    previewDiv.innerHTML = md.render(editor.value);
}

/**
 * Initializes Markdown rendering for the given editors.
 * @param {NodeListOf<HTMLTextAreaElement>} editors - The list of editor elements.
 */
function initializeMarkdownRendering(editors) {
    editors.forEach((editor) => {
        updatePreview(editor);
        editor.addEventListener('input', () => updatePreview(editor));
    });
}

/**
 * Initializes tooltips for the given elements.
 * @param {NodeListOf<Element>} tooltips - The list of tooltip elements.
 */
function initializeTooltips(tooltips) {
    tippy('.tooltip', {
        animation: 'scale',
        theme: 'light',
    });

    tooltips.forEach((tooltip) => {
        const content = tooltip.getAttribute('data-tippy-content');
        if (content) {
            tippy(tooltip, {
                content,
                allowHTML: true,
                theme: 'light',
                animation: 'scale',
            });
        }
    });
}

/**
 * Performs a file download with the given data.
 * @param {BlobPart} data - The data to be downloaded.
 * @param {string} filename - The name of the file.
 * @param {string} type - The MIME type of the file.
 */
function performDownload(data, filename, type) {
    const a = document.createElement('a');
    const blob = new Blob([data], { type });
    a.href = URL.createObjectURL(blob);
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

/**
 * Sets the visibility of the given elements.
 * @param {NodeListOf<Element>} elements - The list of elements to set visibility for.
 * @param {boolean} visibility - The visibility state to set (true for visible, false for hidden).
 */
function setElementVisibility(elements, visibility) {
    elements.forEach((el) => {
        el.style.display = visibility ? 'block' : 'none';
        el.style.visibility = visibility ? 'visible' : 'hidden';
    });
}

/**
 * Exports the canvas as an image.
 * @param {NodeListOf<HTMLTextAreaElement>} editors - The list of editor elements.
 * @param {NodeListOf<Element>} tooltips - The list of tooltip elements.
 */
function exportCanvasAsImage(editors, tooltips) {
    setElementVisibility(editors, false);
    setElementVisibility(tooltips, false);

    html2canvas(document.querySelector('.container'), {
        scale: 2,
        useCORS: true,
    }).then((canvas) => {
        canvas.toBlob((blob) => {
            performDownload(blob, 'LeanCanvas.png', 'image/png');
            setElementVisibility(editors, true);
            setElementVisibility(tooltips, true);
        });
    });
}

/**
 * Exports the canvas as JSON.
 * @param {NodeListOf<HTMLTextAreaElement>} editors - The list of editor elements.
 */
function exportCanvasAsJson(editors) {
    const data = Array.from(editors).map((editor) => ({
        id: editor.id,
        content: editor.value,
    }));
    performDownload(
        JSON.stringify(data),
        'LeanCanvas.json',
        'application/json'
    );
}

/**
 * Export the content of multiple editors as a Markdown file.
 * @param {Array} editors - An array of editor elements.
 */
function exportCanvasAsMarkdown(editors) {
    let markdownContent = '';
    editors.forEach((editor) => {
        const title = editor.parentElement.querySelector('h3').innerText;
        const content = editor.value;
        markdownContent += `# ${title}\n\n${content}\n\n`;
    });
    performDownload(markdownContent, 'canvas.md', 'text/markdown');
}

/**
 * Imports JSON data.
 */
function importJson() {
    const input = document.createElement('input');
    input.type = 'file';
    input.accept = 'application/json';
    input.onchange = (event) => {
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = (event) => {
            const data = JSON.parse(event.target.result);
            data.forEach((item) => {
                const editor = document.getElementById(item.id);
                if (editor) {
                    editor.value = item.content;
                    updatePreview(editor);
                }
            });
        };
        reader.readAsText(file);
    };
    input.click();
}

/**
 * Sets up event listeners for the page.
 */
function setupEventListeners() {
    const editors = Array.from(document.querySelectorAll('.editor'));
    const tooltipElements = document.querySelectorAll('.tooltip, .dropbtn');

    initializeMarkdownRendering(editors);
    initializeTooltips(tooltipElements);

    document
        .getElementById('exportImage')
        .addEventListener('click', () =>
            exportCanvasAsImage(editors, tooltipElements)
        );
    document
        .getElementById('exportJson')
        .addEventListener('click', () => exportCanvasAsJson(editors));
    document
        .getElementById('exportMarkdown')
        .addEventListener('click', () => exportCanvasAsMarkdown(editors));
    document.getElementById('importJson').addEventListener('click', importJson);
}

document.addEventListener('DOMContentLoaded', setupEventListeners);

/**
 * Clears the editors.
 */
function clearEditors() {
    const editors = document.querySelectorAll('.editor');
    editors.forEach((editor) => {
        editor.value = '';
        updatePreview(editor);
    });
}

document.getElementById('clearEditors').addEventListener('click', clearEditors);
